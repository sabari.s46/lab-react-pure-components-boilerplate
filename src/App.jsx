import "./App.css";
import SimpleCounterComponent from "./componets/SimpleCounterComponent";
import PureCounterComponent from "./componets/PureCounterComponent";

function App() {
  return (
    <>
      <SimpleCounterComponent />
      <PureCounterComponent />
    </>
  );
}

export default App;
