A pure component outperforms a simple component by preventing unnecessary re-renders.
