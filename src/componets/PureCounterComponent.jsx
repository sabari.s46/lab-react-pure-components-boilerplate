import React, { Component } from "react";
import { PureComponent } from "react";

export default class PureCounterComponent extends PureComponent {
  constructor(props) {
    super(props);

    // Setting up state
    this.state = {
      count: 0,
      checkCount: false,
    };
  }

  changeCheck = () => {
    this.setState((prevState) => ({
      checkCount: !prevState.checkCount,
    }));
    console.log(this.state.checkCount);
  };

  addCount = () => {
    this.setState({
      count: this.state.toggle ? this.state.count + 1 : this.state.count,
    });
  };

  render() {
    console.log("This is Pure Component");
    const { count } = this.state;
    return (
      <div className="boss">
        <h1>Simple Component</h1>
        <h3>{count}</h3>
        <button onClick={this.changeCheck}>Toggle count</button>
        <button onClick={this.addCount}>Increment count</button>
      </div>
    );
  }
}
