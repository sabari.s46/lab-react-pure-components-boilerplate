import React, { Component } from "react";

export default class SimpleCounterComponent extends Component {
  constructor(props) {
    super(props);

    // Setting up state
    this.state = {
      count: 0,
      checkCount: false,
    };
  }

  changeCheck = () => {
    this.setState((prevState) => ({
      checkCount: !prevState.checkCount,
    }));
  };

  addCount = () => {
    this.setState({
      count: this.state.toggle ? this.state.count + 1 : this.state.count,
    });
  };

  render() {
    console.log("This is Simple Component");
    const { count } = this.state;
    return (
      <div className="boss">
        <h1>Simple Component</h1>
        <h3>{count}</h3>
        <button onClick={this.changeCheck}>Toggle count</button>
        <button onClick={this.addCount}>Increment count</button>
      </div>
    );
  }
}
